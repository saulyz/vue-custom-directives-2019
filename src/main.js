import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// place for defining global directives

new Vue({
  render: h => h(App),
}).$mount('#app')
